<?php

namespace App\Entity;

use App\Db\Database;
use \PDO;

class Products
{
    /**
     * Unique Product ID
     * @var integer
     */
    public $id;

    /**
     * Product code
     * @var string
     */
    public $sku;

    /**
     * Product name
     * @var string
     */
    public $name;

    /**
     * Product price
     * @var float
     */
    public $price;

    /**
     * Product type
     * @var string
     */
    public $poduct;

    /**
     * Product size
     * @var string
     */
    public $size;

    /**
     * Method responsible for registering a new product in the database
     * @return boolean
     */
    public function register()
    {
        $obDatabase = new Database('products');

        $this->id = $obDatabase->insert([
            'sku' => $this->sku,
            'name' => $this->name,
            'price' => $this->price,
            'product' => $this->product,
            'size' => $this->size,
        ]);

        return true;
    }

    /**
     * Method responsible for getting the products from the database
     * @param string $where
     * @param string $order
     * @param string $limit
     * @return array
     */
    public static function getProducts($where = null, $order = null, $limit = null)
    {
        return (new Database('products'))->select($where, $order, $limit)
            ->fetchAll(PDO::FETCH_CLASS, self::class);
    }

    /**
     * Method responsible for deleting the product from the database
     * @return boolean
     */
    public function delete($id)
    {
        $idList = implode(',', $id);

        return (new Database('products'))->delete('id IN (' . $idList . ')');
    }
}
