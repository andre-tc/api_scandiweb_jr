<?php

namespace App\Db;

use \PDO;
use \PDOException;

class Database
{

    /**
     * Database connection host
     * @var string
     */
    const HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const NAME = 'scandiweb';

    /**
     * Database user
     * @var string
     */
    const USER = 'scandiweb';

    /**
     * Database access password
     * @var string
     */
    const PASS = 'scandiweb123';

    /**
     * Name of the table to be manipulated
     * @var string
     */
    private $table;

    /**
     * Database connection instance
     * @var PDO
     */
    private $connection;

    /**
     * Defines the table and instance and connection
     * @param string $table
     */
    public function __construct($table = null)
    {
        $this->table = $table;
        $this->setConnection();
    }

    /**
     * Method responsible for creating a connection to the database
     */
    private function setConnection()
    {
        try {
            $this->connection = new PDO('mysql:host=' . self::HOST . ';dbname=' . self::NAME, self::USER, self::PASS);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }
    }

    /**
     * Method responsible for executing queries inside the database
     * @param string $query
     * @param array $params
     * @return [type]
     */
    public function execute($query, $params = [])
    {
        try {
            $statement = $this->connection->prepare($query);
            $statement->execute($params);
            return $statement;
        } catch (PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }
    }

    /**
     * Method responsible for inserting data in the database
     * @param array $values [field => value]
     * @return integer
     */
    public function insert($values)
    {
        $fields = array_keys($values);
        $binds = array_pad([], count($fields), '?');

        //QUERY MOUNT
        $query = 'INSERT INTO ' . $this->table . ' (' . implode(',', $fields) . ') VALUES (' . implode(',', $binds) . ')';

        //EXECUTES THE INSERT 
        $this->execute($query, array_values($values));

        //RETURNS THE ID ENTERED
        return $this->connection->lastInsertId();
    }

    /**
     * Method responsible for retrieving products from the database
     * @param string $where
     * @param string $order
     * @param string $limit
     * @param string $fields
     * @return PDOStatement
     */
    public function select($where = null, $order = null, $limit = null, $fields = '*')
    {
        //QUERY DATA
        $where = strlen($where) ? 'WHERE ' . $where : '';
        $order = strlen($order) ? 'ORDER BY ' . $order : '';
        $limit = strlen($limit) ? 'LIMIT ' . $limit : '';

        //QUERY MOUNT
        $query = 'SELECT ' . $fields . ' FROM ' . $this->table . ' ' . $where . ' ' . $order . ' ' . $limit;

        //EXECUTES THE QUERY 
        return $this->execute($query);
    }

    /**
     * Method responsible for deleting the product from the databse
     * @param string $where
     * @return boolean
     */
    public function delete($where)
    {
        //QUERY DATA
        $query = 'DELETE FROM ' . $this->table . ' WHERE ' . $where;

        //QUERY MOUNT
        $this->execute($query);

        //RETURN SUCCESS
        return true;
    }
}
