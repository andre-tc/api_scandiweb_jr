<?php

namespace App\Routes;

require 'vendor/autoload.php';

use App\Entity\Products;

class ProductsRoute
{
    /**
     * Route to list all products from database
     * @return object $products
     */
    public function listAll()
    {
        $products = Products::getProducts();

        echo json_encode($products);

        return true;
    }

    /**
     * Route to create a new product on database
     * @return boolean
     */
    public function register()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if (isset($data->sku, $data->name, $data->price, $data->product, $data->size)) {

            $products = Products::getProducts();
            
            foreach ($products as $element) {
                if (strcasecmp($data->sku, $element->sku) == 0) {
                    http_response_code(400);
                    exit;
                }
            }

            $obProduct =  new Products;

            $obProduct->sku = $data->sku;
            $obProduct->name = $data->name;
            $obProduct->price = $data->price;
            $obProduct->product = $data->product;
            $obProduct->size = $data->size;



            $obProduct->register();
            header('location: index.php?status=success');
            return true;
        }
    }

    /**
     * Route to delete one or many products at once from database
     * @param object id = {[num1, num2, ...]}
     * @return boolean
     */
    public function delete()
    {

        $json = file_get_contents('php://input');
        $data = json_decode($json);

        if (!isset($data->id) or !is_array($data->id)) {
            header('location: index.php?status=error');
            exit;
        }

        $products = new Products();
        $products->delete($data->id);

        return true;
    }

    /**
     * Route to invalid url
     * @param string $data
     */
    public function error($data)
    {
        echo "<h1>Error {$data["errocode"]}</h1>";
    }
}
