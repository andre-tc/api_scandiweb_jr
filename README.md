**Api Scandiweb**

This project is an api from Scandiweb junior developer test. 

---

## Necessary installations to run the project

Those are the necessary things to get started with the project.

1. Install php and apach.
2. Install [composer](https://www.hostinger.com/tutorials/how-to-install-composer)
3. Install Mysql as database.

Here is a tutorial to help you to install [php, apache and mysql at linux](https://agaetis.tech/development/how-to-install-linux-server-for-php-and-mysql/)

---

## Where the project must be created

The project must be created inside apache folder to have access to it by localhost.

	1. File_system > var > www > html > create_your_project_here

## Accessing the database.

The **BASE_URL** to access your database is in:
	
	api_scandiwe_jr > app > Routes > Config.php
  
	<?php

		define("URL_BASE", "define_here_your_database_link");
	?>
  
---

## Connecting to database

The connection to database is inside of file Database.php

	`api_scandiweb_jr > Db > Database.php`
	
The default connection to it is:

	const HOST = 'localhost';
    const NAME = 'scandiweb';
    const USER = 'scandiweb';
    const PASS = 'scandiweb123';
	
---

## MySQL creating a table

Run this command after connecting to your database.

	1. mysql> CREATE TABLE pet (
		sku VARCHAR(12), 
		name VARCHAR(50),
		price DECIMAL(10,2), 
		product VARCHAR(30), 
		size VARCHAR(15);

---

## Ready get started.

That's it, now you are ready get started.