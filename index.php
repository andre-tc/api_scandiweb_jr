
<?php
require 'vendor/autoload.php';

use CoffeeCode\Router\Router;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Request-Method: POST, GET, DELETE');

$router = new Router(URL_BASE);

/**
 * Controllers
 */
$router->namespace("App\Routes");

/**
 * PRODUCTS ROUTE
 * home
 */
$router->group(null);
$router->get("/", "ProductsRoute:listAll");
$router->post("/addProduct", "ProductsRoute:register");
$router->post("/deleteProduct", "ProductsRoute:delete");

$router->dispatch();

/**
 * ERRORS
 */
$router->group("ooops");
$router->get("/{errcode}", "ProductsRoute:error");


if ($router->error()) {
    $router->redirect("/ooops/{$router->error()}");
}
